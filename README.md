TWRP device tree for HTC Desire 620 (a31ul)
========================================================

For building TWRP for MSM8916 models only.

Known bugs:
* offmode charging (turns on TWRP UI instead of the charging image)